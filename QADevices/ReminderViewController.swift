//
//  ReminderViewController.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit

class ReminderViewController: UIViewController {

    @IBOutlet weak var messageLabel: SemiBoldLabel!
    @IBOutlet weak var containerView: UIView!
    
    var employeeName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageLabel.text = "Would you like me to remember that you're \(employeeName!)?"
        containerView.heroModifiers = [.translate(x: 0, y: 100, z: 0), .scale(0.5)]
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    @IBAction func stopRemindTapped(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: StoredPreferences.stopReminder.rawValue)
        performSegue(withIdentifier: Segue.unwindToList.rawValue, sender: self)
    }
    
    @IBAction func yesButtonTapped() {
        // TODO: Save their name in user defaults
        UserDefaults.standard.set(employeeName, forKey: StoredPreferences.setEmployeeName.rawValue)
        performSegue(withIdentifier: Segue.unwindToList.rawValue, sender: self)
    }
    
    @IBAction func noButtonTapped() {
        performSegue(withIdentifier: Segue.unwindToList.rawValue, sender: self)
    }
}
