//
//  Theming.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit

struct Colors {
    static let mainTint = DataController.shared.isDarkMode ? UIColor("#4d4d4d") : UIColor("#ececec")
    static let secondaryTint = DataController.shared.isDarkMode ? UIColor("#252525") : UIColor("#F9F9F9")
    static let actionTint = DataController.shared.isDarkMode ? UIColor("#dddddd") : UIColor("#2a2e35")
    static let positiveTint = UIColor("#7acc3e")
    static let negativeTint = UIColor("#df4456")
    static let navBarTint = DataController.shared.isDarkMode ? UIColor("#3a3a3a") : UIColor("#ffffff")
    static let navTitleTint = DataController.shared.isDarkMode ? UIColor("#ededed") : UIColor("#2a2e35")
    static let darkTint = UIColor("#2a2e35")
    static let lightTint = UIColor("#fafafa")
}

extension Colors {
    
    private static let lookup: [String: UIColor] = [
        "mainTint" : mainTint,
        "secondaryTint" : secondaryTint,
        "actionTint": actionTint,
        "positiveTint": positiveTint,
        "negativeTint": negativeTint,
        "navBarTint": navBarTint,
        "navTitleTint": navTitleTint,
        "darkTint": darkTint,
        "lightTint": lightTint
    ]
    
    static func color(named name: String) -> UIColor {
        if let color = lookup[name]{
            return color
        } else {
            fatalError("Unknown color name: \(name)")
        }
    }
}

public extension UIColor {
    
    /// Must be in the format of #AARRGGBB or #RRGGBB (the function will correct pick up the AA components)
    convenience init(_ hexString: String) {
        
        if !(hexString.hasPrefix("#") && (hexString.characters.count == 7 || hexString.characters.count == 9)) {
            fatalError("Incorrect Hex String format")
        }
        
        let scanner = Scanner(string: hexString)
        // Skip #
        scanner.scanString("#", into: nil)
        
        var value: UInt32 = 0
        scanner.scanHexInt32(&value)
        
        var a: UInt32 = 0xFF
        if hexString.characters.count == 9 {
            a = (value & 0xFF000000) >> 24
        }
        let r = (value & 0x00FF0000) >> 16
        let g = (value & 0x0000FF00) >> 8
        let b = (value & 0x000000FF) >> 0
        
        let red = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue = CGFloat(b) / 255.0
        let alpha = CGFloat(a) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

