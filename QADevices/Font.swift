//
//  Font.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit

enum Font {
    
    case light
    case regular
    case medium
    case semiBold
    case bold
    
    func withSize(_ size: CGFloat) -> UIFont {
        
        switch self {
        case .light:
            return UIFont.systemFont(ofSize: size, weight: UIFontWeightLight)
        case .regular:
            return UIFont.systemFont(ofSize: size, weight: UIFontWeightRegular)
        case .medium:
            return UIFont.systemFont(ofSize: size, weight: UIFontWeightMedium)
        case .semiBold:
            return UIFont.systemFont(ofSize: size, weight: UIFontWeightSemibold)
        case .bold:
            return UIFont.systemFont(ofSize: size, weight: UIFontWeightBold)
        }
    }
}
