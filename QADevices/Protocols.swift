//
//  Protocols.swift
//  QADevices
//
//  Created by Jordan.Dixon on 07/07/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import Foundation
import CloudKit

protocol Filterable: class {
    func filter(searchText: String, searchParam: DeviceDBKey) -> [CKRecord]
}

extension Filterable {
    func filter(searchText: String, searchParam: DeviceDBKey) -> [CKRecord] {
        
        var filteredDevices = [CKRecord]()
        let devices = DataController.shared.devices
        
        filteredDevices = devices!.filter { (($0.object(forKey: searchParam.rawValue) as? String)?.range(of: searchText, options: .caseInsensitive) != nil) }
        
        return filteredDevices
    }
}

protocol KeyboardReactable: class {
    var bottomViewConstraint: NSLayoutConstraint? { get }
    func addKeyboardObserver()
}

extension KeyboardReactable where Self: UIViewController {
    func addKeyboardObserver() {
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { notification in
            if let keyboard = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.bottomViewConstraint!.constant = keyboard.height
            }
        }
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { notification in
            self.bottomViewConstraint!.constant = 35
        }
        
        func updateView(from notification: Notification) {

        }
        
    }
}

// MARK: NibInflatable
protocol NibInflatable: class {
    var container: UIView! { get }
    var nibName: String { get }
    func initializeSubviews()
}

extension NibInflatable where Self: UIView {
    
    var nibName: String {
        return String(describing: type(of: self))
    }
    
    func initializeSubviews() {
        let nib = UINib(nibName: nibName, bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            container.leadingAnchor.constraint(equalTo: leadingAnchor),
            container.topAnchor.constraint(equalTo: topAnchor),
            container.trailingAnchor.constraint(equalTo: trailingAnchor),
            container.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
    }
}
