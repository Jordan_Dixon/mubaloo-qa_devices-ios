//
//  EmployeesViewController.swift
//  QADevices
//
//  Created by Jordan.Dixon on 29/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import CloudKit

class EmployeesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var tappedEmployee: String!
    var device: CKRecord!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.heroModifiers = [.cascade]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segue.showReminder.rawValue {
            guard let reminderView = segue.destination as? ReminderViewController else { return }
            reminderView.employeeName = tappedEmployee
        }
    }
}

// MARK: UICollectionViewDelegate
extension EmployeesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataController.shared.employees.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EmployeeCollectionViewCell.reuseID, for: indexPath) as! EmployeeCollectionViewCell
        cell.populate(from: DataController.shared.employees[indexPath.row])
        cell.delegate = self
        cell.heroModifiers = [.fade, .translate(y: 100), .scale(0.7)]
        return cell
    }
}

// MARK: EmployeeCollectionViewCellDelegate
extension EmployeesViewController: EmployeeCollectionViewCellDelegate {
    func cellTapped(employee: String) {

        tappedEmployee = employee
        
        DataController.shared.updateInUseStatus(of: device, for: employee)
        
        guard let shouldStopReminding = UserDefaults.standard.object(forKey: StoredPreferences.stopReminder.rawValue) as? Bool else {
            performSegue(withIdentifier: Segue.showReminder.rawValue, sender: nil)
            return
        }
        
        if shouldStopReminding {
            performSegue(withIdentifier: Segue.unwindToList.rawValue, sender: nil)
        } else {
            performSegue(withIdentifier: Segue.showReminder.rawValue, sender: nil)
        }
    }
}

// MARK: IBAction
extension EmployeesViewController {
    @IBAction func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
}
