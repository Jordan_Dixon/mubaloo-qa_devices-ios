//
//  DataController.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import Foundation
import CloudKit

final class DataController {
    
    static let shared = DataController()
    
    let publicDatabase = CKContainer.default().publicCloudDatabase
    var devices: [CKRecord]!
    var employees: [CKRecord]!
    
    var isDarkMode: Bool {
        return UserDefaults.standard.object(forKey: StoredPreferences.isDarkMode.rawValue) as? Bool ?? false
    }
    
    func save(_ device: Device) {
        
        let newDevice = CKRecord(recordType: "Devices")
        newDevice.setObject(device.name as CKRecordValue?, forKey: DeviceDBKey.deviceName.rawValue)
        newDevice.setObject(device.color as CKRecordValue?, forKey: DeviceDBKey.deviceColour.rawValue)
        newDevice.setObject(device.category as CKRecordValue?, forKey: DeviceDBKey.deviceCategory.rawValue)
        newDevice.setObject(device.operatingSystem as CKRecordValue?, forKey: DeviceDBKey.deviceOS.rawValue)
        newDevice.setObject(device.tag as CKRecordValue?, forKey: DeviceDBKey.deviceTag.rawValue)
        newDevice.setObject("false" as CKRecordValue?, forKey: DeviceDBKey.isDeviceInUse.rawValue)

        publicDatabase.save(newDevice) { [weak self] record, error in
            if error == nil { self?.getAllDevices() }
        }
    }
    
    func updateInUseStatus(of device: CKRecord, for employee: String? = nil) {
        
        if !device.isDeviceInUse {
            device.setObject("true" as CKRecordValue?, forKey: DeviceDBKey.isDeviceInUse.rawValue)
            device.setObject(employee as CKRecordValue?, forKey: DeviceDBKey.inUseEmployee.rawValue)
            device.setObject(Date() as CKRecordValue, forKey: DeviceDBKey.timeTakenOut.rawValue)
        } else {
            device.setObject("" as CKRecordValue?, forKey: DeviceDBKey.inUseEmployee.rawValue)
            device.setObject("false" as CKRecordValue?, forKey: DeviceDBKey.isDeviceInUse.rawValue)
        }
        
        update(device: device)
    }
    
    func update(device: CKRecord) {
        
        publicDatabase.save(device) { [weak self] record, error in
            if error == nil { self?.getAllDevices() }
        }
        
    }
    
}

// MARK: Fetching
extension DataController {

    func getAllDevices() {
        devices = [CKRecord]()
        
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Devices", predicate: predicate)
        query.sortDescriptors = [NSSortDescriptor(key: DeviceDBKey.deviceName.rawValue, ascending: false)]
        
        publicDatabase.perform(query, inZoneWith: nil) { [weak self] records, error in
            
            if error == nil {
                self?.devices = records
                NotificationCenter.default.post(name: .dataFetchComplete, object: nil)
            }
        }
    }
    
    func getAllEmployees() {
        devices = [CKRecord]()
        
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "Employees", predicate: predicate)
        query.sortDescriptors = [NSSortDescriptor(key: "employeeName", ascending: true)]
        
        publicDatabase.perform(query, inZoneWith: nil) { [weak self] records, error in
            
            if error == nil { self?.employees = records }
        }
    }
    
}
