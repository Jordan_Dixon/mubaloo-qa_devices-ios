//
//  FilterCollectionViewController.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import Hero

class FilterCollectionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var isCategoryPicker = false
    var preselectedFilter: String?
    var filterChosen: ((String) -> Void)?
    
    var filterNames: [FilterName] = [.iPhone, .iPad, .androidPhone, .androidTablet, .watch, .iPod, .windows, .client]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.heroModifiers = [.cascade]
        
        if UserDefaults.standard.object(forKey: StoredPreferences.setEmployeeName.rawValue) as? String != nil && !isCategoryPicker {
            filterNames.append(.mine)
        }
        
        if UserDefaults.standard.object(forKey: StoredPreferences.setFilter.rawValue) as? String == nil {
            UserDefaults.standard.set(FilterName.iPhone.rawValue, forKey: StoredPreferences.setFilter.rawValue)
            collectionView.reloadData()
        }
    }
    
    @IBAction func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func superSecretAction() {
        let currentTheme = DataController.shared.isDarkMode
        UserDefaults.standard.set(!currentTheme, forKey: StoredPreferences.isDarkMode.rawValue)
    }
}

extension FilterCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterCollectionViewCell.reuseID, for: indexPath) as! FilterCollectionViewCell
        
        cell.chosen = preselectedFilter
        cell.isCategoryPicker = isCategoryPicker
        cell.populate(from: filterNames[indexPath.row])
        
        cell.cellTapped = { filter in
            
            self.update(with: filter)
            
        }
        return cell
    }
    
    private func update(with filter: String) {
        
        if !isCategoryPicker {
            
            UserDefaults.standard.removeObject(forKey: StoredPreferences.setFilter.rawValue)
            UserDefaults.standard.set(filter, forKey: StoredPreferences.setFilter.rawValue)
            NotificationCenter.default.post(name: .filterChanged, object: nil)
            collectionView.reloadData()
            
        } else {
            filterChosen?(filter)
            collectionView.reloadData()
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
