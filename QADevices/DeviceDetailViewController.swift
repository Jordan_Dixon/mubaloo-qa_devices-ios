//
//  DeviceDetailViewController.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import CloudKit

class DeviceDetailViewController: UIViewController {
    
    @IBOutlet var bottomViewConstraint: NSLayoutConstraint?
    
    @IBOutlet weak var deviceTag: SemiBoldLabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var deviceName: UITextField!
    @IBOutlet weak var deviceOS: UITextField!
    @IBOutlet weak var deviceNotes: UITextView!
    @IBOutlet weak var timeTakenOut: UITextField!
    @IBOutlet weak var employeeName: UITextField!
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var updateStatus: UIButton!
    @IBOutlet weak var dismissButton: CancelButton!
    
    var device: CKRecord!
    var deviceWasChanged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateWith(device: device)
        addKeyboardObservers()
        setupView()
    }
    
    private func setupView() {
        // because hero can't figure out the z position of the views in the heirachy when coming from a cell in the search view controller
        deviceImage.heroModifiers = [.zPosition(10)]
        deviceName.heroModifiers = [.zPosition(10)]
        deviceTag.heroModifiers = [.zPosition(10)]
        deviceNotes.heroModifiers = [.fade, .zPosition(10)]
        deviceOS.heroModifiers = [.zPosition(10)]
        
        let shadow = DataController.shared.isDarkMode ? cellBackgroundView.backgroundColor?.darker(by: 60)?.cgColor : cellBackgroundView.backgroundColor?.darker()?.cgColor
        
        cellBackgroundView.layer.shadowOpacity = 0.65
        cellBackgroundView.layer.shadowColor = shadow
        cellBackgroundView.layer.shadowRadius = 20
    }
    
    private func addKeyboardObservers() {
        
        deviceName.keyboardAppearance = DataController.shared.isDarkMode ? .dark : .light
        deviceOS.keyboardAppearance = DataController.shared.isDarkMode ? .dark : .light
        deviceNotes.keyboardAppearance = DataController.shared.isDarkMode ? .dark : .light

        NotificationCenter.default.addObserver(forName: .UIKeyboardWillShow, object: nil, queue: nil) { notification in
            if let keyboard = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.bottomViewConstraint!.constant = keyboard.height + 10
            }
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
        
        NotificationCenter.default.addObserver(forName: .UIKeyboardWillHide, object: nil, queue: nil) { notification in
            self.bottomViewConstraint!.constant = 35
            
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func populateWith(device: CKRecord) {
        
        deviceName.text = device.deviceName
        deviceTag.text = device.deviceTag
        deviceOS.text = device.deviceOS
        deviceNotes.text = device.deviceNotes
        
        if !device.isDeviceInUse {
            cellBackgroundView.backgroundColor = Colors.positiveTint
            employeeName.text = ""
            updateStatus.backgroundColor = Colors.negativeTint
            timeTakenOut.removeFromSuperview()
        } else {
            cellBackgroundView.backgroundColor = Colors.negativeTint
            employeeName.text = device.inUseEmployee
            updateStatus.backgroundColor = Colors.positiveTint
            setTime()
        }
        
        guard let deviceCategory = device.deviceCategory,
            let deviceColour = device.deviceColor else { return }
        
        let deviceImageName = "\(deviceCategory) \(deviceColour)"
        deviceImage.image = UIImage(named: deviceImageName)
    }
    
    private func setTime() {
        
        if let dateTakenOut = device.timeTakenOut {
            
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .full
            
            if dateTakenOut.timeIntervalSinceNow < -60 {
                formatter.allowedUnits = [.day, .hour, .minute]
            } else {
                formatter.allowedUnits = [.second]
            }
            
            if var time = formatter.string(from: dateTakenOut.timeIntervalSinceNow) {
                time.characters.removeFirst()
                timeTakenOut?.text = "\(time) ago"
            }
        }
        
    }
}

// MARK: IBAction
extension DeviceDetailViewController {
    
    @IBAction func closeView(_ sender: Any) {
        
        if deviceWasChanged {
            DataController.shared.update(device: device)
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeStatus(_ sender: UIButton) {
        
        if device.isDeviceInUse {
            DataController.shared.updateInUseStatus(of: device)
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        guard let employee = UserDefaults.standard.object(forKey: StoredPreferences.setEmployeeName.rawValue) as? String else {
            performSegue(withIdentifier: Segue.showEmployees.rawValue, sender: nil)
            return
        }
        
        DataController.shared.updateInUseStatus(of: device, for: employee)
        self.dismiss(animated: true, completion: nil)
        
    }
}

// MARK: Segue
extension DeviceDetailViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
            
        case Segue.showEmployees.rawValue?:
            guard let employeeVC = segue.destination as? EmployeesViewController else { return }
            employeeVC.device = device
        default: return
        }
    }
}

// MARK: UITextFieldDelegate
extension DeviceDetailViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        deviceWasChanged = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField {
            
        case deviceName:
            device.setObject(textField.text as CKRecordValue?, forKey: DeviceDBKey.deviceName.rawValue)
        case deviceOS:
            device.setObject(textField.text as CKRecordValue?, forKey: DeviceDBKey.deviceOS.rawValue)
        default: return
        }
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}

// MARK: UITextViewDelegate
extension DeviceDetailViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        deviceWasChanged = true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        device.setObject(textView.text as CKRecordValue?, forKey: DeviceDBKey.deviceNote.rawValue)
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
