//
//  Extensions.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit

public extension UIView {
    
    @IBInspectable var backgroundColorName: String? {
        get {
            fatalError("This accessor method should not be called")
        }
        set {
            if let name = newValue {
                backgroundColor = Colors.color(named: name)
            } else {
                backgroundColor = nil
            }
        }
    }
    
    @IBInspectable var tintColorName: String? {
        get {
            fatalError("This accessor method should not be called")
        }
        set {
            if let name = newValue {
                tintColor = Colors.color(named: name)
            } else {
                tintColor = nil
            }
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable public var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable public var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        
        set {
            layer.shadowOffset = newValue
        }
    }
}

extension UITextField {
    @IBInspectable var textColorName: String? {
        get {
            fatalError("This accessor should not be called")
        }
        
        set {
            if let name = newValue {
                
                let color = Colors.color(named: name)
                
                textColor = color
                
                guard let placeholderColor = DataController.shared.isDarkMode ? color.darker(by: 40) : color.lighter(by: 40) else { return }
                
                attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSForegroundColorAttributeName: placeholderColor])
            } else {
                textColor = nil
            }
        }
    }
}

extension UIButton {
    @IBInspectable var textColorName: String? {
        get {
            fatalError("This accessor should not be called")
        }
        
        set {
            if let name = newValue {
                setTitleColor(Colors.color(named: name), for: .normal)
            }
        }
    }
}
