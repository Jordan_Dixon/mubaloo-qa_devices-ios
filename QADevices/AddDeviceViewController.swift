//
//  AddDeviceViewController.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import Hero

class AddDeviceViewController: UIViewController {
    
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var deviceNameLabel: UITextField!
    @IBOutlet weak var deviceOSLabel: UITextField!
    @IBOutlet weak var deviceCategoryButton: UIButton!
    @IBOutlet weak var deviceTag: UITextField!
    
    @IBOutlet weak var blackButton: UIButton!
    @IBOutlet weak var whiteButton: UIButton!
    @IBOutlet weak var actionStack: UIStackView!
    
    var device = Device()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
}

extension AddDeviceViewController {
    
    fileprivate func setupView() {
        
        deviceNameLabel.keyboardAppearance = DataController.shared.isDarkMode ? .dark : .light
        deviceOSLabel.keyboardAppearance = DataController.shared.isDarkMode ? .dark : .light
        deviceTag.keyboardAppearance = DataController.shared.isDarkMode ? .dark : .light
        
        device.color = "black"
        colourButtonTapped(blackButton)
        deviceNameLabel.heroModifiers = [.fade]
        deviceOSLabel.heroModifiers = [.fade]
        deviceTag.heroModifiers = [.fade]
        blackButton.heroModifiers = [.fade]
        whiteButton.heroModifiers = [.fade]
        addView.heroModifiers = [.fade, .scale(0.5)]
        
        actionStack.heroModifiers = [.fade, .translate(y: 400), .scale(0.3)]
    }
    
    fileprivate func showMissingTextFields() {
        let labels = [deviceNameLabel, deviceOSLabel, deviceTag]
        
        for label in labels {
            if label?.text == "" {
                label?.layer.borderColor = Colors.negativeTint.cgColor
                label?.layer.borderWidth = 2.0
            } else {
                label?.layer.borderColor = UIColor.clear.cgColor
            }
        }

        if device.category == nil {
            deviceCategoryButton.layer.borderColor = Colors.negativeTint.cgColor
            deviceCategoryButton.layer.borderWidth = 2.0
        } else {
            deviceCategoryButton.layer.borderColor = UIColor.clear.cgColor
            
        }
    }
}

extension AddDeviceViewController {
    
    @IBAction func saveDevice() {
        
        if deviceNameLabel.text != "" && deviceOSLabel.text != "" && deviceTag.text != "" && device.category != nil {
            storeDevice()
            DataController.shared.save(device)
            self.dismiss(animated: true, completion: nil)
        } else {
            showMissingTextFields()
        }
    }
    
    @IBAction func cancelTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func colourButtonTapped(_ sender: UIButton) {
        switch sender.titleLabel!.text! {
        case "Black":
            whiteButton.backgroundColor = UIColor.clear
            whiteButton.setTitleColor(Colors.navTitleTint, for: .normal)
            blackButton.backgroundColor = Colors.darkTint
            blackButton.setTitleColor(Colors.lightTint, for: .normal)
            device.color = "black"
        case "White":
            blackButton.backgroundColor = UIColor.clear
            blackButton.setTitleColor(Colors.navTitleTint, for: .normal)
            whiteButton.backgroundColor = Colors.lightTint
            whiteButton.setTitleColor(Colors.darkTint, for: .normal)
            device.color = "white"
        default: break
        }
    }
    
}

extension AddDeviceViewController {
    
    fileprivate func storeDevice() {
        device.name = deviceNameLabel.text
        device.operatingSystem = deviceOSLabel.text
        device.tag = deviceTag.text
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Segue.categoryPicker.rawValue {
            
            guard let destination = segue.destination as? FilterCollectionViewController else { return }
            
            view.endEditing(true)
            
            destination.isCategoryPicker = true
            destination.preselectedFilter = device.category
            
            destination.filterChosen = { category in
                self.device.category = category
                self.deviceCategoryButton.setTitle(category, for: .normal)
            }
            
            storeDevice()
        }
    }
    
}

// MARK: UITextFieldDelegate
extension AddDeviceViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
