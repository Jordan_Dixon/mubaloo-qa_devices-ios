//
//  EmployeeCollectionViewCell.swift
//  QADevices
//
//  Created by Jordan.Dixon on 29/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import CloudKit

protocol EmployeeCollectionViewCellDelegate {
    func cellTapped(employee: String)
}

class EmployeeCollectionViewCell: UICollectionViewCell {
    
    var delegate: EmployeeCollectionViewCellDelegate!
    static let reuseID = "EmployeeCell"
    
    @IBOutlet weak var employeeName: SemiBoldLabel!
    
    @IBAction func employeeTapped(_ sender: UIButton) {
        delegate.cellTapped(employee: employeeName.text ?? "")
    }
    
    func populate(from employee: CKRecord) {
        employeeName.text = employee.value(forKey:"employeeName") as? String
    }
}
