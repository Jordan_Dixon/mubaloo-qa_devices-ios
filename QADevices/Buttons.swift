//
//  Buttons.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit

class AddButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    private func customInit() {
        backgroundColor = Colors.darkTint
        tintColor = .white
        setImage(UIImage(named: "plus"), for: .normal)
        layer.cornerRadius = frame.size.width / 2.0
        layer.shadowOpacity = 0.4
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    }
}

class SearchButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    private func customInit() {
        backgroundColor = Colors.darkTint
        tintColor = .white
        setImage(UIImage(named: "search"), for: .normal)
        layer.cornerRadius = frame.size.width / 2.0
        layer.shadowOpacity = 0.4
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    }
}

class CancelButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    private func customInit() {
        backgroundColor = Colors.actionTint
        tintColor = Colors.secondaryTint
        setImage(UIImage(named: "cancel"), for: .normal)
        layer.cornerRadius = frame.size.width / 2.0
    }
}

class FilterButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    private func customInit() {
        backgroundColor = Colors.mainTint
        tintColor = Colors.actionTint
        setImage(UIImage(named: "filter"), for: .normal)
        layer.cornerRadius = frame.size.width / 2.0
    }
}

class DismissButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    @IBInspectable var text: String? {
        didSet {
            self.setTitle(text, for: .normal)
        }
    }
    
    private func customInit() {
        backgroundColor = Colors.negativeTint
        tintColor = .white
        setTitle("Cancel", for: .normal)
        setTitleColor(UIColor.white, for: .normal)
        layer.cornerRadius = 10.0
    }
}

class SaveButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    @IBInspectable var text: String? {
        didSet {
            self.setTitle(text, for: .normal)
        }
    }
    
    private func customInit() {
        backgroundColor = Colors.positiveTint
        tintColor = .white
        setTitle("Save", for: .normal)
        setTitleColor(UIColor.white, for: .normal)
        layer.cornerRadius = 10.0
    }
}

class CheckboxButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    private func customInit() {
        tintColor = .white
        setTitle("Don't ask me again", for: .normal)
        setTitleColor(Colors.actionTint, for: .normal)
        setTitleColor(Colors.secondaryTint, for: .selected)
        layer.cornerRadius = 10.0
    }
}

class SearchTypeButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    override var isSelected: Bool {
        didSet {
            updateView()
        }
    }
    
    private func customInit() {
        layer.cornerRadius = 15.0
        self.setTitleColor(Colors.lightTint, for: .selected)
        self.setTitleColor(Colors.navTitleTint, for: .normal)
        updateView()
    }
    
    private func updateView() {
        if isSelected {
            self.backgroundColor = Colors.positiveTint
        } else {
            self.backgroundColor = .clear
        }
    }
}
