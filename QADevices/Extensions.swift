//
//  Extensions.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import CloudKit

extension Notification.Name {
    static let dataFetchComplete = Notification.Name("dataFetchComplete")
    static let filterChanged = Notification.Name("filterChaned")
}

extension String {
    
    var boolValue: Bool {
        return self == "true"
    }
}

extension UIColor {
    
    func lighter(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: abs(percentage))
    }
    
    func darker(by percentage: CGFloat = 30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage))
    }
    
    func adjust(by percentage: CGFloat = 30.0) -> UIColor? {
        
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        
        if (self.getRed(&r, green: &g, blue: &b, alpha: &a)) {
            return UIColor(red: min(r + percentage / 100, 1.0),
                           green: min(g + percentage / 100, 1.0),
                           blue: min(b + percentage / 100, 1.0),
                           alpha: a)
        } else {
            return nil
        }
        
    }
    
}

extension CKRecord {
    var deviceName: String? {
        return self.value(forKey: DeviceDBKey.deviceName.rawValue) as? String
    }
    
    var deviceOS: String? {
        return self.value(forKey: DeviceDBKey.deviceOS.rawValue) as? String
    }
    
    var deviceTag: String? {
        return self.value(forKey: DeviceDBKey.deviceTag.rawValue) as? String
    }
    
    var deviceNotes: String? {
        return self.value(forKey: DeviceDBKey.deviceNote.rawValue) as? String
    }
    
    var isDeviceInUse: Bool {
        guard let isDeviceInUseString = self.value(forKey: DeviceDBKey.isDeviceInUse.rawValue) as? String else {
            return false
        }
        
        return isDeviceInUseString.boolValue
    }
    
    var deviceColor: String? {
        return self.value(forKey: DeviceDBKey.deviceColour.rawValue) as? String
    }
    
    var deviceCategory: String? {
        return self.value(forKey: DeviceDBKey.deviceCategory.rawValue) as? String
    }
    
    var timeTakenOut: Date? {
        return self.value(forKey: DeviceDBKey.timeTakenOut.rawValue) as? Date
    }
    
    var inUseEmployee: String? {
        return self.value(forKey: DeviceDBKey.inUseEmployee.rawValue) as? String
    }
}
