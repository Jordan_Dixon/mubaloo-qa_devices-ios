//
//  ViewController.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import CloudKit
import Hero

class DeviceCollectionViewController: UIViewController, Filterable {
    
    @IBOutlet weak var categoryName: BoldLabel?
    @IBOutlet weak var collectionView: UICollectionView?
    @IBOutlet weak var addButton: AddButton?
    @IBOutlet weak var searchButton: SearchButton!
    @IBOutlet weak var filterButton: FilterButton!
    
    @IBOutlet weak var searchButtonBottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var addButtonBottomConstraint: NSLayoutConstraint?
    
    // Search mode
    @IBOutlet weak var searchField: UITextField?
    @IBOutlet weak var nameSearchButton: SearchTypeButton?
    @IBOutlet weak var osSearchButton: SearchTypeButton?
    @IBOutlet weak var tagSearchButton: SearchTypeButton?
    
    var buttons = [String: UIButton?]()
    
    var indexPath: IndexPath?
    var isSearch = false
    var searchType: DeviceDBKey?
    var devices: [CKRecord]?
    var searchTypes: [DeviceDBKey] = [.deviceName, .deviceOS, .deviceTag]
    var refreshControl: UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshDevices()
        resetHero()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

// MARK: View setup
extension DeviceCollectionViewController {
    
    func setupView() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateView), name: .filterChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateView), name: .dataFetchComplete, object: nil)
        
        let filterPreference = UserDefaults.standard.object(forKey: StoredPreferences.setFilter.rawValue) as? String ?? "iPhone"
        self.categoryName?.text = filterPreference
        
        searchButton?.heroModifiers = [.translate(y: 100)]
        addButton?.heroModifiers = [.translate(y: 100)]
        filterButton?.heroModifiers = [.translate(x: 100)]
        
        collectionView?.register(DeviceCollectionViewCell.self, forCellWithReuseIdentifier: DeviceCollectionViewCell.reuseID)
        
        collectionView?.heroModifiers = [.cascade]
        
        if !isSearch {
            
            self.collectionView?.alwaysBounceVertical = true
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector(refreshDevices), for: .valueChanged)
            collectionView?.addSubview(refreshControl!)
            
        } else {
            
            searchField?.keyboardAppearance = DataController.shared.isDarkMode ? .dark : .light
            
            searchField?.delegate = self
            
            buttons = ["Name": nameSearchButton, "OS": osSearchButton, "Tag": tagSearchButton]
            searchType = .deviceName
            
            searchField?.heroModifiers = [.fade]
            nameSearchButton?.heroModifiers = [.fade, .scale(0.5)]
            osSearchButton?.heroModifiers = [.fade, .scale(0.5)]
            tagSearchButton?.heroModifiers = [.fade, .scale(0.5)]
            
            if let searchField = searchField {
                self.devices = filter(searchText: searchField.text!, searchParam: searchType!)
                self.collectionView?.reloadData()
            }
            
        }
    }
    
    @objc fileprivate func refreshDevices() {
        DataController.shared.getAllDevices()
    }
    
    func updateView() {
        
        guard let _ = DataController.shared.devices else { return }
        
        DispatchQueue.main.async {
            
            let filterPreference = UserDefaults.standard.object(forKey: StoredPreferences.setFilter.rawValue) as? String ?? "iPhone"
            self.categoryName?.text = filterPreference
            if !self.isSearch {
                
                if filterPreference == FilterName.mine.rawValue {
                    guard let name = UserDefaults.standard.object(forKey: StoredPreferences.setEmployeeName.rawValue) as? String else { return }
                    self.devices = self.filter(searchText: name, searchParam: .inUseEmployee)
                } else {
                    self.devices = self.filter(searchText: filterPreference, searchParam: .deviceCategory)
                }
            }
            
            self.collectionView?.reloadData()
            
            self.refreshControl?.endRefreshing()

        }
    }

}

// MARK: Segue
extension DeviceCollectionViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case Segue.deviceDetailView.rawValue?:
            guard let detailView = segue.destination as? DeviceDetailViewController else { return }
            
            searchField?.resignFirstResponder()
            
            let cell = sender as! DeviceCollectionViewCell
            let indexPath = collectionView?.indexPath(for: cell)
            let collectionCell = collectionView?.cellForItem(at: indexPath!) as! DeviceCollectionViewCell
            collectionCell.heroModifiers = [.fade]
            detailView.device = self.devices![indexPath!.row]
            
        case Segue.searchView.rawValue?:
            guard let searchView = segue.destination as? DeviceCollectionViewController else { return }
            searchView.isSearch = true

        default: return
        }
    }

}

// MARK: IBActions
extension DeviceCollectionViewController {
    
    @IBAction func searchButtonTapped(sender: SearchTypeButton) {
        turnOn(button: sender)
    }
    
    @IBAction func cancelSearch() {
        
        searchField?.resignFirstResponder()
        
        resetHero()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unwindToList(segue: UIStoryboardSegue) { }
    
    func turnOn(button: SearchTypeButton) {
        
        guard let nameButton = nameSearchButton,
            let osButton = osSearchButton,
            let tagButton = tagSearchButton else { return }
        
        nameButton.isSelected = false
        osButton.isSelected = false
        tagButton.isSelected = false
        
        button.isSelected = true
        
        switch button {
            
        case nameButton:
            searchField?.keyboardType = .alphabet
            searchType = .deviceName
            
        case osButton:
            searchField?.keyboardType = .decimalPad
            searchType = .deviceOS
            
        case tagButton:
            searchField?.keyboardType = .numberPad
            searchType = .deviceTag

        default: break
        }
        
        if let searchField = searchField {
            self.devices = filter(searchText: searchField.text!, searchParam: searchType!)
            // to swap the keyboard types
            searchField.resignFirstResponder()
            searchField.becomeFirstResponder()
        }
        
        self.collectionView?.reloadData()
    }
}

// MARK: UITextFieldDelegate
extension DeviceCollectionViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let searchField = searchField,
            let searchString = searchField.text as NSString? else { return true }
        
        let newText = searchString.replacingCharacters(in: range, with: string)
        
        self.devices = filter(searchText: newText as String, searchParam: searchType!)
        self.collectionView?.reloadData()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: Scoll view delegate
extension DeviceCollectionViewController {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        searchField?.resignFirstResponder()
        
        let scrollViewHeight = scrollView.frame.size.height
        let scrollContentSizeHeight = scrollView.contentSize.height
        let scrollOffset = scrollView.contentOffset.y
        
        if scrollContentSizeHeight > view.frame.height {
            
            throwButtons(away: scrollOffset + scrollViewHeight >= scrollContentSizeHeight)
        }
        
    }
    
    fileprivate func throwButtons(away shouldHide: Bool) {
        
        if shouldHide {
            addButtonBottomConstraint?.constant = -70
            searchButtonBottomConstraint?.constant = -70
        } else {
            addButtonBottomConstraint?.constant = 20
            searchButtonBottomConstraint?.constant = 20
        }
        
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.7, options: [UIViewAnimationOptions.allowUserInteraction], animations: {
            self.view.layoutIfNeeded()
        })
    }
    
}

// MARK: Collection view delegate
extension DeviceCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.devices?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DeviceCollectionViewCell.reuseID, for: indexPath) as! DeviceCollectionViewCell
        
        let deviceRecord = self.devices![indexPath.row]
        
        cell.populate(with: deviceRecord, heroModifier: [.fade, .translate(y: 50), .scale(0.8)], isSegue: self.indexPath?.row == indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        resetHero()
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? DeviceCollectionViewCell else { return }
        cell.setHero()
        self.indexPath = indexPath
        
        performSegue(withIdentifier: Segue.deviceDetailView.rawValue, sender: cell)
    }
    
    fileprivate func resetHero() {
        if let lastIndex = self.indexPath,
            let cell = collectionView?.cellForItem(at: lastIndex) as? DeviceCollectionViewCell {
            cell.resetHero()
            self.indexPath = nil
        }
    }

}

// MARK: Collection view flow layout delegate
extension DeviceCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        
        flowLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: bestWidthForCell(), height: 290)
    }
    
    // this is either good or the dumbest thing i've ever done in my life
    func bestWidthForCell(previousCellWidth: CGFloat? = nil) -> CGFloat {
        
        let rightLeftPadding: CGFloat = 22.0
        let maximumCellWidth: CGFloat = 185.0
        let minimumCellWidth: CGFloat = 130.0
        
        var currentCellWidth: CGFloat = minimumCellWidth
        
        if let previousCellWidth = previousCellWidth {
            currentCellWidth = previousCellWidth + 1
        }
        
        // expand the width until the width of the frame / the width + padding is really close to an integer (e.g. 768 (iPad width) / (170 + 22) = 4, meaning 4 cells can fit perfectly in the view)
        while (view.frame.width / (currentCellWidth + rightLeftPadding)).truncatingRemainder(dividingBy: 1) > 0.01 {
            currentCellWidth += 1
        }
        
        // if the currentCellWidth is smaller than the max cell width (it could have just found the local optimum), try again, else just return the max width
        if currentCellWidth < maximumCellWidth {
            return bestWidthForCell(previousCellWidth: currentCellWidth)
        } else {
            return previousCellWidth ?? maximumCellWidth
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(15, 15, 15, 15)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
}
