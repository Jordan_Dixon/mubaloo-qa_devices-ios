//
//  DataStructures.swift
//  QADevices
//
//  Created by Jordan Dixon on 08/07/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import Foundation

enum FilterName: String {
    case iPhone
    case iPad
    case iPod
    case androidPhone = "Android Phone"
    case androidTablet = "Android Tablet"
    case watch = "Watch"
    case windows = "Windows"
    case client = "Client Device"
    case mine = "Mine"
}

enum StoredPreferences: String {
    case setEmployeeName
    case setFilter
    case stopReminder
    case isDarkMode
}

enum DeviceDBKey: String {
    case deviceName
    case deviceCategory
    case deviceNote
    case deviceOS
    case deviceTag
    case deviceColour
    case inUseEmployee
    case isDeviceInUse
    case timeTakenOut
    
    var humanName: String {
        switch self {
        case .deviceName:
            return "Name"
        case .deviceOS:
            return "OS"
        case .deviceTag:
            return "Tag"
        default:
            return "Other"
        }
    }
}

struct Device {
    var name: String?
    var color: String?
    var category: String?
    var operatingSystem: String?
    var tag: String?
}
