//
//  Storyboard.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import Foundation

enum Segue: String {
    case deviceDetailView
    case deviceCollectionView
    case categoryPicker
    case searchView
    case showEmployees
    case showReminder
    case unwindToList
    case showFilters
}

enum HeroKey: String {
    case DeviceTag
    case DeviceName
    case DeviceImage
    case EmployeeName
    case CellBackground
    case BackgroundView
    case ContextLabel
    case DeviceOS
    case MainButton
    case SecondaryButton
    case ThirdButton
    case FilterLabel
    case Employee
}
