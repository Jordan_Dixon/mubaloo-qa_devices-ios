//
//  DeviceCollectionViewCell.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import CloudKit
import Hero

class DeviceCollectionViewCell: UICollectionViewCell, NibInflatable {
    
    static let reuseID = "DeviceCell"
    
    @IBOutlet var container: UIView!
    
    @IBOutlet weak var deviceTag: SemiBoldLabel!
    @IBOutlet weak var deviceImage: UIImageView!
    @IBOutlet weak var employeeName: UILabel!
    @IBOutlet weak var deviceName: UILabel!
    @IBOutlet weak var operatingSystem: UILabel!
    @IBOutlet weak var cellBackgroundView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
}

// MARK: Population
extension DeviceCollectionViewCell {
    
    func populate(with device: CKRecord, heroModifier: [HeroModifier], isSegue: Bool) {
        
        deviceName.text = device.deviceName
        deviceTag.text = device.deviceTag
        operatingSystem.text = device.deviceOS
        
        cellBackgroundView.heroModifiers = heroModifier
        
        if !device.isDeviceInUse {
            cellBackgroundView.backgroundColor = Colors.positiveTint
            employeeName.text = ""
        } else {
            cellBackgroundView.backgroundColor = Colors.negativeTint
            employeeName.text = device.value(forKey: DeviceDBKey.inUseEmployee.rawValue) as? String
        }
        
        // set up heroIDs for animation to this cell
        isSegue ? setHero() : resetHero()
        
        if let deviceCategory = device.deviceCategory,
            let deviceColour = device.deviceColor {
            
            let deviceImageName = "\(deviceCategory) \(deviceColour)"
            deviceImage.image = UIImage(named: deviceImageName)
        } else {
            deviceImage.removeFromSuperview()
        }
        
    }
    
    func setHero() {
        deviceTag.heroID = HeroKey.DeviceTag.rawValue
        deviceName.heroID = HeroKey.DeviceName.rawValue
        deviceImage.heroID = HeroKey.DeviceImage.rawValue
        employeeName.heroID = HeroKey.EmployeeName.rawValue
        cellBackgroundView.heroID = HeroKey.CellBackground.rawValue
        operatingSystem.heroID = HeroKey.DeviceOS.rawValue
        
        cellBackgroundView.heroModifiers = [.zPosition(0)]
    }
    
    func resetHero() {
        deviceTag.heroID = ""
        deviceName.heroID = ""
        deviceImage.heroID = ""
        employeeName.heroID = ""
        cellBackgroundView.heroID = ""
        operatingSystem.heroID = ""
    }
}
