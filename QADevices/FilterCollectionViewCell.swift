//
//  FilterCollectionViewCell.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit
import Hero

class FilterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var filterName: SemiBoldLabel!
    
    var isCategoryPicker = false
    var chosen: String?
    var cellTapped: ((String) -> Void)?
    
    static let reuseID = "FilterCell"
    
    func populate(from filter: FilterName) {
        filterName.text = filter.rawValue

        var chosen: String?
        
        if !isCategoryPicker {
            chosen = UserDefaults.standard.object(forKey: StoredPreferences.setFilter.rawValue) as? String
        } else {
            chosen = self.chosen
        }
        
        if chosen == filter.rawValue {
            filterName.backgroundColor = Colors.positiveTint
            filterName.textColor = Colors.secondaryTint
            self.heroModifiers = [.translate(y: 100), .scale(0.5)]
        } else {
            filterName.backgroundColor = UIColor.clear
            filterName.textColor = Colors.actionTint
            self.heroModifiers = [.translate(y: 100), .scale(0.5)]
        }
    }
    
    @IBAction func cellTapped(_ sender: UIButton) {
        
        guard let filter = filterName.text else { return }
        
        cellTapped?(filter)
    }
}
