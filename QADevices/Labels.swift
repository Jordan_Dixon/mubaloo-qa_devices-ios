//
//  Labels.swift
//  QADevices
//
//  Created by Jordan.Dixon on 28/01/2017.
//  Copyright © 2017 Jordan.Dixon. All rights reserved.
//

import UIKit

@IBDesignable
class _Label: UILabel {
    
    var fontStyle: Font {
        fatalError("A font must be returning in subclasses")
    }
    
    @IBInspectable var textTint: String? {
        didSet {
            self.textColor = Colors.color(named: textTint!)
        }
    }
    
    @IBInspectable var fontPointSize: CGFloat {
        get {
            return font.pointSize
        }
        set {
            super.font = fontStyle.withSize(newValue)
        }
    }
    
    override var font: UIFont! {
        set {
            fontPointSize = font.pointSize
        }
        get {
            return super.font
        }
    }
}

// MARK: - Concrete label
class LightLabel: _Label {
    override var fontStyle: Font {
        return .light
    }
}

class RegularLabel: _Label {
    override var fontStyle: Font {
        return .regular
    }
}

class MediumLabel: _Label {
    override var fontStyle: Font {
        return .medium
    }
}

class SemiBoldLabel: _Label {
    override var fontStyle: Font {
        return .semiBold
    }
}

class BoldLabel: _Label {
    override var fontStyle: Font {
        return .bold
    }
}
